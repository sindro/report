$(function() {
    function requestData(chart, url) {
        $.ajax({
            type: "GET",
            url: url,
        })
            .done(function (data) {
                chart.setData(data);
            })
            .fail(function () {
                alert("Error");
            });
    }

    var chart = Morris.Line({
        element: 'chart',
        xkey: 'createdAt',
        ykeys: ['amount'],
        labels: ['Amount']
    });

    requestData(chart, $('#chart').data('url'));
});