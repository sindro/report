<?php
namespace App\Controller;

use App\Entity\Customer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route(name="app_index", path="/")
     */
    public function index()
    {
        $customers = $this->getDoctrine()->getRepository(Customer::class)->findAll();

        return $this->render('Default/index.html.twig', ['customers' => $customers]);
    }

    /**
     * @Route(name="app_show", path="/customer/{customer}/transactions")
     */
    public function show(Request $request, Customer $customer)
    {
        $transactions = $customer->getTransactionsByCurrency('EUR');

        if($request->isXmlHttpRequest()) {
            return $this->getJsonTransactions($transactions);
        }

        return $this->render('Default/show.html.twig', [
            'customer' => $customer,
            'transactions' => $transactions
        ]);
    }

    /**
     * @param $transactions
     * @return JsonResponse
     * Restituisce una risposta json con la data di creazione e il totale della transizione
     */
    private function getJsonTransactions($transactions)
    {
        $customer_transactions = [];

        foreach ($transactions as $transaction) {
            $customer_transactions[] = [
                'createdAt' => $transaction->getCreatedAt()->format('Y-m-d'),
                'amount' => (float) $transaction->getAmount()
            ];
        }

        return new JsonResponse($customer_transactions);
    }
}