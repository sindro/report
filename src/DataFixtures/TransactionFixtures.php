<?php
namespace App\DataFixtures;

use App\Entity\Currency;
use App\Entity\Customer;
use App\Entity\Transaction;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TransactionFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * Carica i dati nella tabella transizioni
     */
    public function load(ObjectManager $manager)
    {
        $transactions = [
            ['a.gregoletto.mabon@gmail.com', "01/04/2015", 'GBP', 50.00],
            ['a.gregoletto.mabon@gmail.com', "02/04/2015", 'GBP', 11.04],
            ['a.gregoletto.mabon@gmail.com', "02/04/2015", 'EUR', 1.00],
            ['a.gregoletto.mabon@gmail.com', "03/04/2015", 'USD', 23.05],
            ['mariorossi@gmail.com', "01/04/2015", 'USD', 66.10],
            ['mariorossi@gmail.com', "02/04/2015", 'EUR', 12.00],
            ['mariorossi@gmail.com', "02/04/2015", 'GBP',  6.50],
            ['mariorossi@gmail.com', "04/04/2015", 'EUR', 6.50]
        ];

        foreach ($transactions as $t)
        {
            /**
             * customer della transizione
             */
            $customer = $manager->getRepository(Customer::class)->findOneBy(['email' => $t[0]]);
            /**
             * trasforma la stringa in data
             */
            $createdAt= new \DateTime(date('Y-m-d', strtotime($t[1])));
            /**
             * currency della transizione
             */
            $currency = $manager->getRepository(Currency::class)->findOneBy(['code' => $t[2]]);

            /**
             * crea la transizione
             */
            $transaction = new Transaction();
            $transaction->setCurrency($currency);
            $transaction->setAmount($t[3]);
            $transaction->setCreatedAt($createdAt);
            $transaction->setCustomer($customer);
            $manager->persist($transaction);
        }

        $manager->persist($customer);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            CurrencyFixtures::class,
            CustomerFixtures::class,
        );
    }
}

