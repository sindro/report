<?php
namespace App\DataFixtures;

use App\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CustomerFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * Carica i dati nella tabella Customer
     */
    public function load(ObjectManager $manager)
    {
        $customers = [
            ['Alessandro', 'Gregoletto', 'a.gregoletto.mabon@gmail.com'],
            ['Mario', 'Rossi', 'mariorossi@gmail.com']
        ];

        foreach ($customers as $c)
        {
            $customer = new Customer();
            $customer->setFirstname($c[0]);
            $customer->setLastname($c[1]);
            $customer->setEmail($c[2]);

            $manager->persist($customer);
        }

        $manager->flush();
    }
}