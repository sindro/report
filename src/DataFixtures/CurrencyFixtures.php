<?php
namespace App\DataFixtures;

use App\Entity\Currency;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrencyFixtures extends Fixture
{
    /**
     * @param ObjectManager $manager
     * Carica i dati nella tabella Currency
     */
    public function load(ObjectManager $manager)
    {
        $currencyCode = ['EUR', 'USD', 'GBP'];

        foreach ($currencyCode as $code)
        {
            $currency = new Currency();
            $currency->setCode($code);
            $manager->persist($currency);
        }

        $manager->flush();
    }
}