<?php

namespace App\Command;

use App\Entity\Customer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Doctrine\Common\Persistence\ObjectManager;

class TransactionCommand extends Command
{
    protected static $defaultName = 'app:transaction';
    private $manager;

    public function __construct(ObjectManager $manager)
    {

        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:transaction')
            ->setDescription('Restituisce le transizioni dell\'utente')
            ->setDefinition(array(
                new InputArgument('customer_id', InputArgument::REQUIRED, 'Customer ID'),
                new InputOption('currency-code', null, InputOption::VALUE_OPTIONAL, 'Set Currency Code'),
            ))
            ->setHelp(<<<'EOT'
                Il comando <info>app:transaction</info> restituisce le transizioni dell'utente
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $customer_id = $input->getArgument('customer_id');
        $currency_code = $input->getOption('currency-code') ?: 'EUR';

        /**
         * restituisce il Customer
         */
        $customer = $this->manager->getRepository(Customer::class)->findOneBy(['id' => $customer_id]);

        if(!$customer || $customer->getTransactions()->count() == 0 ) {
            $output->writeln(sprintf('Nessun dato disponibile per Customer ID <comment>%s</comment>', $customer_id));
        } else {

            $transactions = $customer->getTransactionsByCurrency($currency_code);

            $table = new Table($output);
            $table
                ->setHeaders(array('Customer ID', 'Customer Info', 'Transaction Date', 'Transaction Amount'))
                ->setRows($this->getTableRows($transactions))
            ;
            $table->render();
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = array();

        if (!$input->getArgument('customer_id')) {
            $question = new Question('Customer ID:');
            $question->setValidator(function ($customer_id) {
                if (empty($customer_id)) {
                    throw new \Exception('Customer ID must be defined');
                }

                return $customer_id;
            });
            $questions['customer_id'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param $transactions
     * @return array con le transizioni del customer
     */
    private function getTableRows($transactions)
    {
        $rows = [];

        foreach($transactions as $transaction)
        {
            $rows[] = [
                $transaction->getCustomer()->getId(),
                $transaction->getCustomer()->getFullName() . ' (' . $transaction->getCustomer()->getEmail() . ')',
                $transaction->getCreatedAt()->format('d-m-Y'),
                twig_localized_currency_filter($transaction->getAmount(), $transaction->getCurrency())
            ];
        }

        return $rows;
    }
}