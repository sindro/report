<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
     * Testa la homepage
     */
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals(2, $crawler->filter('ul > li')->count());
    }

    /**
     * Testa la pagina del customer con id 1
     */
    public function testShowFirstCustomer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customer/1/transactions');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(
            0,
            $crawler->filter('p:contains("Transizioni di Alessandro Gregoletto (a.gregoletto.mabon@gmail.com)")')->count()
        );

        $this->assertEquals(1, $crawler->filter('div#chart')->count());
        $this->assertEquals(1, $crawler->filter('ul > li')->count());
    }

    /**
     * Testa la pagina del customer con id 2
     */
    public function testShowSecondCustomer()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/customer/2/transactions');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(
            0,
            $crawler->filter('p:contains("Transizioni di Mario Rossi (mariorossi@gmail.com)")')->count()
        );

        $this->assertEquals(1, $crawler->filter('div#chart')->count());
        $this->assertEquals(2, $crawler->filter('ul > li')->count());
    }
}