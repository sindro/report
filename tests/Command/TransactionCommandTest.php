<?php
namespace App\Tests\Command;

use App\Entity\Currency;
use App\Entity\Customer;
use App\Entity\Transaction;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use App\Command\TransactionCommand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TransactionCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        /**
         * mock del customer repository
         */
        $customerRepository = $this->createMock(ObjectRepository::class);
        $customerRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($this->getCustomer());

        /**
         * mock object manager che restituirà il mock del customer repository
         */
        $objectManager = $this->createMock(ObjectManager::class);
        $objectManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($customerRepository);

        $application = new Application($kernel);
        $application->add(new TransactionCommand($objectManager));

        $command = $application->find('app:transaction');
        $commandTester = new CommandTester($command);
        $commandTester->execute(
            array('customer_id' => 1)
        );

        $this->assertContains("+-------------+------------------------------------------------------+------------------+--------------------+
| Customer ID | Customer Info                                        | Transaction Date | Transaction Amount |
+-------------+------------------------------------------------------+------------------+--------------------+
| 1           | Alessandro Gregoletto (a.gregoletto.mabon@gmail.com) | 13-03-2018       | 10,00 €            |
+-------------+--------------------------------------------------", $commandTester->getDisplay());

    }

    /**
     * @return FakeCustomer
     */
    private function getCustomer()
    {
        $customer = new FakeCustomer();
        $customer->setId(1);
        $customer->setFirstname('Alessandro');
        $customer->setLastname('Gregoletto');
        $customer->setEmail('a.gregoletto.mabon@gmail.com');
        $customer->setTransactions([$this->getTransaction()]);

        return $customer;
    }

    /**
     * @return Transaction
     */
    private function getTransaction()
    {
        $transaction = new Transaction();
        $transaction->setCurrency($this->getCurrency());
        $transaction->setCreatedAt(new \DateTime(date('2018-03-13')));
        $transaction->setAmount(10.00);

        return $transaction;
    }

    /**
     * @return Currency
     */
    private function getCurrency()
    {
        $currency = new Currency();
        $currency->setCode('EUR');

        return $currency;
    }
}

class FakeCustomer extends Customer
{
    public function setId($id)
    {
        $this->id = $id;
    }
}