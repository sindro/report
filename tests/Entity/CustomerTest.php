<?php
namespace App\Tests\Entity;

use App\Entity\Transaction;
use PHPUnit\Framework\TestCase;

class CustomerTest extends TestCase
{
    public function testFirstname()
    {
        $customer = $this->getCustomer();
        $customer->setFirstname('Alessandro');

        $this->assertSame($customer->getFirstname(), 'Alessandro');
    }

    public function testLastname()
    {
        $customer = $this->getCustomer();
        $customer->setLastname('Gregoletto');

        $this->assertSame($customer->getLastname(), 'Gregoletto');
    }

    public function testFullName()
    {
        $customer = $this->getCustomer();
        $customer->setFirstname('Alessandro');
        $customer->setLastname('Gregoletto');

        $this->assertSame($customer->getFullName(), 'Alessandro Gregoletto');
    }

    public function testEmail()
    {
        $customer = $this->getCustomer();
        $customer->setEmail('a.gregoletto.mabon@gmail.com');

        $this->assertSame($customer->getEmail(), 'a.gregoletto.mabon@gmail.com');
    }

    public function testAddTransactions()
    {
        $customer = $this->getCustomer();
        $customer->addTransaction($this->getTransaction());

        $this->assertSame($customer->getTransactions()->count(), 1);
    }

    public function testSetTransactions()
    {
        $transaction1 = $this->getTransaction();
        $transaction2 = $this->getTransaction();

        $customer = $this->getCustomer();
        $customer->setTransactions([$transaction1, $transaction2]);

        $this->assertSame($customer->getTransactions()->count(), 2);
    }

    public function testRemoveTransaction()
    {
        $transaction1 = $this->getTransaction();
        $transaction2 = $this->getTransaction();

        $customer = $this->getCustomer();
        $customer->setTransactions([$transaction1, $transaction2]);

        $customer->removeTransaction($transaction1);

        $this->assertSame($customer->getTransactions()->count(), 1);
    }

    public function testGetTransactionsByCurrency()
    {
        $customer = $this->getCustomer();
        $customer->addTransaction($this->getTransaction());

        $this->assertSame($customer->getTransactionsByCurrency('EUR')->count(), 1);
    }

    /**
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->getMockForAbstractClass('App\Entity\Customer');
    }

    /**
     * @return Currency
     */
    protected function getCurrency()
    {
        $currency = $this->getMockForAbstractClass('App\Entity\Currency');
        $currency->setCode('EUR');

        return $currency;
    }

    /**
     * @return Transaction
     */
    protected function getTransaction()
    {
        $transaction =  $this->getMockForAbstractClass('App\Entity\Transaction');
        $transaction->setAmount(10.00);
        $transaction->setCurrency($this->getCurrency());

        return $transaction;
    }
}