<?php
namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;

class CurrencyTest extends TestCase
{
    public function testCurrency()
    {
        $currency = $this->getCurrency();
        $currency->setCode('EUR');

        $this->assertSame($currency->getCode(), 'EUR');
    }

    /**
     * @return Currency
     */
    protected function getCurrency()
    {
        return $this->getMockForAbstractClass('App\Entity\Currency');
    }
}