<?php
namespace App\Tests\Entity;

use PHPUnit\Framework\TestCase;

class TransactionTest extends TestCase
{
    public function testCurrency()
    {
        $currency = $this->getMockForAbstractClass('App\Entity\Currency');
        $currency->setCode('EUR');

        $transaction = $this->getTransaction();
        $transaction->setCurrency($currency);

        $this->assertSame($transaction->getCurrency(), $currency);
    }

    public function testCreatedAt()
    {
        $createdAt = new \DateTime(date('Y-m-d'));

        $transaction = $this->getTransaction();
        $transaction->setCreatedAt($createdAt);

        $this->assertSame($transaction->getCreatedAt(), $createdAt);
    }

    public function testAmount()
    {
        $transaction = $this->getTransaction();
        $transaction->setAmount(10.00);

        $this->assertSame($transaction->getAmount(), 10.00);
    }

    public function testCustomer()
    {
        $customer = $this->getCustomer();
        $customer->setFirstname('Alessandro');
        $customer->setLastname('Gregoletto');
        $customer->setEmail('a.gregoletto.mabon@gmail.com');

        $transaction = $this->getTransaction();
        $transaction->setCustomer($customer);

        $this->assertSame($transaction->getCustomer(), $customer);
    }

    /**
     * @return Transaction
     */
    protected function getTransaction()
    {
        return $this->getMockForAbstractClass('App\Entity\Transaction');
    }

    /**
     * @return Customer
     */
    protected function getCustomer()
    {
        return $this->getMockForAbstractClass('App\Entity\Customer');
    }
}