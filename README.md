Report Transizioni
===============================
Il task è stato realizzato con il framework symfony 4.0.6, di seguito troverete tutti i passaggi per la configurazione.

### Primo passaggio
Installare tutte le dipendenze tramite composer 

### Secondo passaggio
Configurazione e creazione del database<br/>
Caricare le fixtures attraverso il comando "php bin/console doctrine:fixtures:load"

### Comando Console
Richiesta: Creare un semplice report che mostri le transazioni per un customer id
specificato come argomento da linea di comando.
Il report dovrà mostrare i valori in EUR.

Risposta: Per visualizzare le transizioni il comando da console è:<strong>php bin/console app:transaction</strong><br/>
Verrà richiesto di inserire il campo obbligatorio <strong>Customer id</strong>, una volta inserito verrà restituita una tabella con le transizioni in euro dell'utente.
<br/>Specificando l'opzione <strong>--currency-code=EUR</strong> o <strong>--currency-code=GBP</strong> o <strong>--currency-code=USD</strong>, verrano restituite le transizioni con la valuta appropriata.

### Frontend
Realizzazione di una lista con i customer<br/>
Realizzazione di una grafico con la libreria morris.js, riceve una risposta json con data di creazione e totale di ogni transizione in euro per ogni customer presente

### Test
Cambiare i parametri del db nel phpunit.xml.dist<br/>
<strong>php bin/phpunit</strong>

